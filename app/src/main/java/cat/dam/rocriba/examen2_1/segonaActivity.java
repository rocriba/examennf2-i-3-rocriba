package cat.dam.rocriba.examen2_1;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;



public class segonaActivity extends AppCompatActivity {

    @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.winner_layout);

        //Declarem variables
        EditText nom = (EditText) findViewById(R.id.et_nom);
        EditText adreca = (EditText) findViewById(R.id.et_adreca);
        String snom = nom.getText().toString();
        String sadreca = adreca.getText().toString();

        EditText talla = (EditText) findViewById(R.id.et_talla);
        EditText color = (EditText) findViewById(R.id.et_color);
        String stralla = talla.getText().toString();
        String scolor = color.getText().toString();
        ImageView samarreta = (ImageView) findViewById(R.id.iv_samarreta);

        //En cas de canviar el color canviem la samarreta
        if (scolor == "red"){
            samarreta.setImageResource(R.drawable.binarytshirt_red);
        } else if (scolor == "blue") {
            samarreta.setImageResource(R.drawable.binarytshirt_blue);
        } else if (scolor == "black") {
            samarreta.setImageResource(R.drawable.binarytshirt_black);
        }


        //Fem el boto per enviar dades amb toast
        final Button enviar = (Button) findViewById(R.id.btn_enviar);
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Nom: " + snom + " Adreça: " + sadreca + " talla: " + stralla + " color: " + color, Toast.LENGTH_SHORT).show();

                //Canviem d'activitat
                Intent intent = new Intent(segonaActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }
}
