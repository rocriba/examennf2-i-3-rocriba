package cat.dam.rocriba.examen2_1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private int numero_random;
    private int contador = 0;

    ToggleButton bin1;
    ToggleButton bin2;
    ToggleButton bin3;
    ToggleButton bin4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Declaració de variables
        TextView numero = (TextView) findViewById(R.id.tv_numero);
        TextView encerts = (TextView) findViewById(R.id.tv_encerts);
        Button boto_validar = (Button) findViewById(R.id.btn_validar);
         bin1 = (ToggleButton) findViewById(R.id.tbtn_bin1);
         bin2 = (ToggleButton) findViewById(R.id.tbtn_bin2);
         bin3 = (ToggleButton) findViewById(R.id.tbtn_bin3);
         bin4 = (ToggleButton) findViewById(R.id.tbtn_bin4);
        Rand();

        //fem random
        numero.setText(String.valueOf(numero_random));
        //Quan l'usuari ha hagi clicat els toggle buttons comprovarem si està bé, peràixò creem un boto per saber quan l'usuari
        //ha acabat
        boto_validar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ( contador == 0){
                    Intent intent = new Intent(MainActivity.this, segonaActivity.class);
                    startActivity(intent);
                }

                if (validarBotons()) {
                    contador++;
                    encerts.setText("Encerts: " + contador);
                    Rand();
                    numero.setText(String.valueOf(numero_random));
                } else {
                    contador = 0;
                    encerts.setText("Encerts: " + contador);
                    Rand();
                    numero.setText(String.valueOf(numero_random));
                }

                }


        });




    }

    private void Rand(){
        numero_random = new Random().nextInt(15 );
    }

    private boolean validarBotons() {
        int num = 0;
        if (bin1.isChecked()) {
            num = num + 1;
        }
        if (bin2.isChecked()) {
            num = num + 2;
        }
        if (bin3.isChecked()) {
            num = num + 4;
        }
        if (bin4.isChecked()) {
            num = num + 8;
        }

        if (numero_random == num) return true;
        else return false;

    }
}